package com.example.memorygame;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.os.Bundle;
import android.widget.TextView;

import java.util.*;


public class MainActivity extends AppCompatActivity {

    int FacingUp = 0;
    Button LastClicked;
    Boolean TurnOver = false;
    int megtalalt = 0;

    TextView szoveg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        List<String> toLetters = new ArrayList<>();
        toLetters.add("A");
        toLetters.add("A");
        toLetters.add("B");
        toLetters.add("B");
        toLetters.add("C");
        toLetters.add("C");
        toLetters.add("D");
        toLetters.add("D");
        toLetters.add("E");
        toLetters.add("E");
        toLetters.add("F");
        toLetters.add("F");
        Collections.shuffle(toLetters);
        String[] letters = new String[toLetters.size()];
        letters = toLetters.toArray(letters);


        Button buttons[] = {(Button) findViewById(R.id.button1),
                (Button) findViewById(R.id.button2),
                (Button) findViewById(R.id.button3),
                (Button) findViewById(R.id.button4),
                (Button) findViewById(R.id.button5),
                (Button) findViewById(R.id.button6),
                (Button) findViewById(R.id.button7),
                (Button) findViewById(R.id.button8),
                (Button) findViewById(R.id.button9),
                (Button) findViewById(R.id.button10),
                (Button) findViewById(R.id.button11),
                (Button) findViewById(R.id.button12)};

        szoveg = findViewById(R.id.textView);
        //szoveg.setText("0");

        findViewById(R.id.RestartButton).setEnabled(false);
        findViewById(R.id.RestartButton).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Restart();
            }
        });


        for(int i = 0; i < 12; i++) {
            buttons[i].setText(letters[i]);
            buttons[i].setTextScaleX(0);
            buttons[i].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Button b = (Button)v;
                    if (b.getTextScaleX() == 0 && !TurnOver){
                        b.setTextScaleX(1);
                        FacingUp++;
                        //szoveg.setText(String.valueOf(FacingUp));
                        if (FacingUp == 1){
                            LastClicked = (Button)v;
                        }
                        if (FacingUp == 2){
                            TurnOver = true;
                            if (b.getText() == LastClicked.getText()){
                                b.setClickable(false);
                                LastClicked.setClickable(false);
                                FacingUp = 0;
                                //szoveg.setText(String.valueOf(FacingUp));
                                megtalalt += 2;
                                if (megtalalt == 12){
                                    findViewById(R.id.RestartButton).setEnabled(true);
                                }
                                TurnOver = false;
                            }
                        }
                    }
                    else if (b.getTextScaleX() == 1){
                        b.setTextScaleX(0);
                        FacingUp--;
                        //szoveg.setText(String.valueOf(FacingUp));
                        if (FacingUp == 0){
                            TurnOver = false;
                        }
                    }
                }
            });

        }
    }

    public void Restart (){
        finish();
        overridePendingTransition(0, 0);
        startActivity(getIntent());
        overridePendingTransition(0, 0);
    }
}